import 'package:flutter/material.dart';
import 'package:lab12flutter/models/option_model.dart';

class MenuOptionsScreen extends StatefulWidget {
  @override
  _MenuOptionsScreenState createState() => _MenuOptionsScreenState();
}

class _MenuOptionsScreenState extends State<MenuOptionsScreen> {
  int _selectedOption = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFAFAFA),
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text('Menu de Opciones'),
      ),
      body: ListView.builder(
        itemCount: options.length,
        itemBuilder: (BuildContext context, int index) {
          if (index == options.length) {
            return SizedBox(height: 15.0);
          }
          return Container(
            alignment: Alignment.center,
            margin: EdgeInsets.all(5.0),
            width: double.infinity,
            height: 80.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10.0),
              border: _selectedOption == index
                  ? Border.all(color: Colors.black26)
                  : null,
            ),
            child: ListTile(
              leading: options[index].icon,
              title: Text(
                options[index].title,
                style: TextStyle(
                  color: _selectedOption == index
                      ? Colors.black
                      : Colors.grey[600],
                ),
              ),
              subtitle: Text(
                options[index].subtitle,
                style: TextStyle(
                  color:
                      _selectedOption == index ? Colors.black : Colors.grey,
                ),
              ),
              selected: _selectedOption == index,
              onTap: () {
                setState(() {
                  _selectedOption = index;
                });
              },
            ),
          );
        },
      ),
    );
  }
}
